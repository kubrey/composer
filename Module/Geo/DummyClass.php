<?php

namespace Module\Geo;

/**
 * Description of DummyClass
 *
 * @author kubrey <kubrey@gmail.com>
 */
class DummyClass {
    public function __construct() {
        
    }
    
    /**
     * 
     */
    public static function doStuff(){
        echo uniqid();
    }
}
